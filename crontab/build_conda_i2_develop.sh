#! /bin/bash

# là où est find_version_and_replace.py
PY_DIR=/home/iota2/iota2-devtools/crontab

# là où on va télécharger iota2
WORKING_DIR=/home/iota2/crontab/build/
PACKAGE_DIR=/home/iota2/crontab/packages
# proxy
# PROXY_FILE=/mnt/data/home/tardyb/config_proxy.txt
export GIT_LFS_SKIP_SMUDGE=1
# conda
CONDA_BIN=/home/iota2/miniconda3/bin
CONDA_SH=/home/iota2/miniconda3/etc/profile.d/conda.sh
CONDA_BUILD=/home/iota2/miniconda3/conda-bld/linux-64
if [ -d "$WORKING_DIR" ]; then
  rm -rf "$WORKING_DIR"
fi

mkdir "$WORKING_DIR"
OLD_PWD=$PWD

#source "$PROXY_FILE"
cd "$WORKING_DIR"

# get conda
export PATH="$CONDA_BIN:$PATH"
. "$CONDA_SH"

git clone https://framagit.org/iota2-project/iota2.git -b develop

cd "$WORKING_DIR"/iota2/install/recipes

commit_id=$(git log --format="%H" -n 1)
short_commit_id="${commit_id:0:8}"

python "$PY_DIR"/find_version_and_replace.py "$WORKING_DIR"/iota2/install/recipes/iota2/meta.yaml "$short_commit_id"

# conda build ! purge all vs purge ? que choisir...

echo "purge"
conda-build purge-all
echo "start build"
conda-build iota2
mkdir "$PACKAGE_DIR"/"$short_commit_id"/
cp -r "$CONDA_BUILD" "$PACKAGE_DIR"/"$short_commit_id"/.
cp -r "$CONDA_BUILD"/../noarch "$PACKAGE_DIR"/"$short_commit_id"/.
cp "$CONDA_BUILD"/../channeldata.json "$PACKAGE_DIR"/"$short_commit_id"/.
cd $OLD_PWD


