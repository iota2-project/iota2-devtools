#! /bin/bash

# conda
CONDA_BIN=/home/iota2/miniconda3/bin
CONDA_SH=/home/iota2/miniconda3/etc/profile.d/conda.sh

# get conda
export PATH="$CONDA_BIN:$PATH"
. "$CONDA_SH"

conda activate iota2-nightly

cd /home/iota2/crontab/build/iota2
# build documentation
# -w add branch can be multiple !expr reg develop can match merge_develop...
# -W add tag can be multiple ! expr reg
sphinx-versioning build -r master doc/source doc/build/html

scp -r doc/build/html/* oso@osr-cesbio.ups-tlse.fr:/media/sf_Data_VM/donneeswww/TheiaOSO/iota2_documentation/

conda deactivate
# rm -rf doc/build
