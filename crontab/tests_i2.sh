#! /bin/bash

TEST_DIR=/home/iota2/crontab/tests/nightly_i2_tests
CONDA_ENV_PATH=/home/iota2/miniconda3/envs/iota2-env-nightly-tests
CUR_PWD=$PWD
# DATA_DIRECTORY=/home/iota2/test_iota2_data/data
# PROXY_FILE=/mnt/data/home/vincenta/set_proxy.sh

echo "DEBUT DES TESTS"
if [[ -d "$TEST_DIR" ]]
then
    rm -rf "$TEST_DIR"
fi

mkdir -p "$TEST_DIR"

# source "$PROXY_FILE"

cd "$TEST_DIR"
git clone https://framagit.org/iota2-project/iota2.git -b develop

export IOTA2DIR="$TEST_DIR"/iota2

# cd "$TEST_DIR"
# cp -r "$DATA_DIRECTORY" "$TEST_DIR"/iota2

# activate conda

CONDA_BIN=/home/iota2/miniconda3/bin
CONDA_SH=/home/iota2/miniconda3/etc/profile.d/conda.sh

# get conda
export PATH="$CONDA_BIN:$PATH"
. "$CONDA_SH"
conda activate iota2-nightly

if [[ -L "$CONDA_ENV_PATH"/lib/python3.6/site-packages/iota2 ]]
then
    rm -r "$CONDA_ENV_PATH"/lib/python3.6/site-packages/iota2
fi

ln -s "$TEST_DIR"/iota2/iota2 "$CONDA_ENV_PATH"/lib/python3.6/site-packages

#~ cd "$TEST_DIR"/iota2/iota2/Tests/UnitTests
cd "$TEST_DIR"/iota2/iota2/Tests

# launch unittests
#~ python -m unittest discover ./ -p "*Tests*.py"
python launch_tests.py -tests_suite all


#~ python -m unittest running_Tests.iota_tests_runs_case.test_s2_scikit_learn > "$TEST_DIR"/tests_results.txt
#~ python -m unittest running_Tests.iota_tests_runs_case.test_s2_scikit_learn

#~ tar jcvf "$TEST_DIR"/tests_results.tar.bz2 "$TEST_DIR"/tests_results.txt

#~ tardyb@cesbio.cnes.fr
#~ echo TEST | mailx -s "Test3 smtp depuis S2CALC" yann.robert@obs-mip.fr
#~ mailx -a "$TEST_DIR"/tests_results.tar.bz2 -s "i2_tests_results" tardyb@cesbio.cnes.fr

conda deactivate
cd $CUR_PWD
unset IOTA2DIR
