#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


def update_i2_pkg_version(i2_meta, new_version):
    """update i2 recipe meta.yaml file according to new version string
    """
    version = '{% set version = "' + str(new_version) + '" %}'
    file_content = [version]
    with open(i2_meta, "r+") as i2_file:
        #print(i2_file)
        #i2_file.next()
        next(i2_file)
        for line in i2_file:
            file_content.append(line)
    with open(i2_meta, "w") as i2_file:
        i2_file.write("\n".join(file_content))


if __name__ == "__main__":
    META_FILE = sys.argv[1]
    NEW_COMMIT_ID = sys.argv[2]
    update_i2_pkg_version(META_FILE, NEW_COMMIT_ID)
