#! /bin/bash

unittest_report_dir=/mnt/data/home/vincenta

. /mnt/data/home/vincenta/cron/tests_i2.sh > "$unittest_report_dir"/unittest_report.txt 2>&1


tar jcvf "$unittest_report_dir"/unittest_report.tar.bz2 "$unittest_report_dir"/unittest_report.txt

mailx -a "$unittest_report_dir"/unittest_report.tar.bz2 -s "i2_tests_results" tardyb@cesbio.cnes.fr < /dev/null
