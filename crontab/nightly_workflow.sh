#! /bin/bash


# lance compilation
COMPIL_SH=/home/iota2/iota2-devtools/crontab/build_conda_i2_develop.sh
. "$COMPIL_SH"
# si env existe supprime
conda env remove -n iota2-nightly
# fait l'install
conda install -n iota2-nightly iota2_develop --use-local  -c /home/iota2/miniconda3/conda-bld/linux-64
# ajoute sphinx-versionning
pip install git+https://github.com/leokoppel/sphinxcontrib-versioning.git

# lance la doc
DOC_SH=/home/iota2/iota2-devtools/crontab/build_doc.sh
. "$DOC_SH"
# lance les tests
TESTS_SH=/home/iota2/iota2-devtools/crontab/test_i2.sh
